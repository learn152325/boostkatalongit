<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_Comment_comment</name>
   <tag></tag>
   <elementGuidId>007b5b92-7e8a-4b8f-80c3-231d08449988</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#txt_comment</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='txt_comment']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>ba4bc314-ce08-4b47-9358-7cca87fc1345</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>7e80bafe-1316-404f-89a4-91de2ec060bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>txt_comment</value>
      <webElementGuid>257e0d6e-0a74-449e-8f6a-7eaddb859096</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>comment</value>
      <webElementGuid>825d47f0-a807-439b-9412-5d440b042207</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Comment</value>
      <webElementGuid>581fcc1a-b8ea-470e-b724-de23d82ac983</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>e21abc01-a3b3-4612-90f7-44cfef735d20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;txt_comment&quot;)</value>
      <webElementGuid>f6be2222-453c-493b-9f27-79fa30baadf7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='txt_comment']</value>
      <webElementGuid>516b9d96-b69a-48ac-b397-275135f66c7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[5]/div/textarea</value>
      <webElementGuid>e5943494-2a65-4533-8f34-0e93db312605</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>f18e24df-4dd4-41b2-941f-7da2c15e1147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'txt_comment' and @name = 'comment' and @placeholder = 'Comment']</value>
      <webElementGuid>b09d4450-7e7b-4755-8b4a-6fd7a1927c23</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
