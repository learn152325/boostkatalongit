<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>myProfilePage</name>
   <tag></tag>
   <elementGuidId>87299713-915b-4fe8-ba32-13e6bfbccfe9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='profile']/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>60490e9b-a6df-489a-8f38-10fcbbfccf9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
      <webElementGuid>f6388865-86c9-4dee-a8b0-5a251d5dfb94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                Profile
                Under construction.
                Logout
            
        </value>
      <webElementGuid>62b7f8fd-378b-4fa9-bb9e-61604d0b8367</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;profile&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]</value>
      <webElementGuid>217451fb-816e-4e33-aed9-636ece912004</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='profile']/div/div</value>
      <webElementGuid>f7aaf38c-a0a5-43f0-8f58-48f78b03ba79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::div[2]</value>
      <webElementGuid>e9d8619f-797c-4e6b-9774-a1f6b58e5333</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::div[2]</value>
      <webElementGuid>7eb64d78-49dd-4380-a9b5-580b0cd53651</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>859ee4c6-7b7d-4cac-a019-a8b8290f9db0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
                Profile
                Under construction.
                Logout
            
        ' or . = '
            
                Profile
                Under construction.
                Logout
            
        ')]</value>
      <webElementGuid>3e9d81e2-9ba4-4a3a-a35f-e0656e71d113</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
